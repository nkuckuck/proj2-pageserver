from flask import Flask, render_template, abort

app = Flask(__name__)

@app.route("/<name>")
def hello(name):
    try:
        return render_template(name)
    except: 
        if "~" in name or ".." in name or "//" in name:
            abort(403)
        else:
            abort(404)

@app.errorhandler(403)
def forbidden_error(error):
    return render_template('403.html')

@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
